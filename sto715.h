#ifndef STO715
#define STO715

#include "pipe_seam.h"

//#define _USE_MATH_DEFINES
#include <cmath>
#include <QDebug>

# define M_PI		3.14159265358979323846	/* pi */
# define M_E		2.7182818284590452354	/* e */

#include <cassert>
#include <tuple>

namespace sto715
{
/* Приложение Б
 * Алгоритмы схематизации одиночных и групповых дефектов сварного соединения */
namespace B {
// Б 1. Алгоритмы схематизации одиночных дефектов кольцевого стыкового сварного соединения МГ
// Б.1.2 Одиночные внутренние дефекты
void checkDefect(Defect &defect)
{
    if (defect.h1 <= 0) defect.h1 = defect.ll;
    if (defect.h2 <= 0) defect.h2 = defect.lt;

    auto F = defect.F;
    if (F > 0) {
        defect.c = sqrt(2*F/M_PI);
        defect.a = sqrt(F/2*M_PI);
    }
    else {
        defect.c = defect.h1/2.f;
        defect.a = defect.h2/2.f;
    }
}
// Б.1.3 Одиночные поверхностные дефекты
// Б.1.4 Одиночные сквозные дефекты
// Б.2. Алгоритмы схематизации групповых дефектов кольцевого стыкового сварного сое-
// динения МГ при оценке первого уровня
// таблица Б.1 пункт 7-9
Defect isNotComplanar(const Defect &defect1, const Defect &defect2)
{
    auto type1 = defect1.type_place;
    auto type2 = defect2.type_place;
    auto ll1 = defect1.ll;
    auto ll2 = defect2.ll;
    auto lt1 = defect1.lt;
    auto lt2 = defect2.lt;
    auto l1 = std::abs(defect1.x0 + ll1 - defect2.x0);
    auto l2 = std::abs(defect1.y0 + lt2 - defect2.y0);
    auto l3 = std::abs(defect1.x0 + l1 - (defect2.x0 + l2));

    auto h1 = defect1.h;
    auto h2 = defect2.h;

    // п.7 Внутренние некомпланарные дефекты
    if (type1 == TYPE::IN && type2 == TYPE::IN) {
        if (l2 <= (h1+h2)/2 && l1 <= std::min(ll1, ll2) && l3 <= std::min(lt1, lt2)) {
            auto H = h1 + h2 + l2;
            auto L = ll1 + ll2 + l1;
            return Defect(defect1, defect2, H, L);
        }
        // п.8 Внутренние некомпланарные дефекты
        else if (l2 <= (h1+h2)/2 && l1 <= std::min(ll1, ll2) && l3 <= std::min(lt1, lt2)) {
            auto H = h1 + h2 + l2;
            auto L = lt1 + lt2 + l3;
            return Defect(defect1, defect2, H, L);
        }
    }
    // п.9 Внутренний и поверхностный некомпланарные дефекты
    else if ((type1 == TYPE::IN && type2 == TYPE::SURFACE) ||
             (type1 == TYPE::SURFACE && type2 == TYPE::IN)) {
        return Defect();
    }
    return Defect();
}
// таблица Б.1 пункт 1-6
Defect checkDefect(const Defect &defect1, const Defect &defect2)
{
    auto defect = isNotComplanar(defect1, defect2);
    if (defect.type_unite == TYPE_UNITE::UNITE)
        return defect;

    auto type1 = defect1.type_place;
    auto type2 = defect2.type_place;
    auto ll1 = defect1.ll;
    auto ll2 = defect2.ll;
    auto l = std::abs(defect1.x0 + ll1 - defect2.x0);
    auto h1 = defect1.h;
    auto h2 = defect2.h;

    // 1 Поверхностные компланарные дефекты
    if (type1 == TYPE::SURFACE && type2 == TYPE::SURFACE) {
        if (l <= std::min(ll1, ll2)) {
            auto H = std::max(h1, h2);
            auto L = ll1 + ll2 + l;
            return Defect(defect1, defect2, H, L);
        }
    }
    // 2 Внутренние компланарные дефекты
    else if (type1 == TYPE::IN && type2 == TYPE::IN) {
        if (l <= (h1 + h2)/2) {
            auto H = h1 + h2 + l;
            auto L = std::max(ll1, ll2);
            return Defect(defect1, defect2, H, L);
        }
    }
    return Defect();
}
}

/* Приложение Е
 * Правила определения расчетных напряжений */
namespace E {
using sigma_x_t = std::pair<double, double>;
/* Номинальные напряжения
 * sigma_N_x - продольная компонента
 * sigma_N_teta - кольцевая компонента */
sigma_x_t calcNominalStress(const PipeSeam &seam)
{
    // FIXME pipe1?
    auto pipe1 = seam.pipe1;
    auto p = seam.p;
    auto Dvn = pipe1.getDvn();
    auto t = pipe1.t;
    auto alfa_t = seam.alfa_t;
    auto E = pipe1.E;
    auto deltaT = seam.deltaT;
    auto Dn = pipe1.Dn;
    auto ro = seam.ro;

    assert(t);
    auto sigma_teta = p*Dvn/(2*t);
    auto v = seam.v;
    assert(ro);
    auto sigma_x = v*sigma_teta - alfa_t*E*deltaT + (E*Dn/(2*ro));
    return {sigma_x, sigma_teta};
}
/* Локальные напряжения
 * sigma_L_x - продольная компонента
 * sigma_L_teta - кольцевая компонента */
sigma_x_t calcLocalStress(const PipeSeam &seam)
{
    auto pipe1 = seam.pipe1;
    auto pipe2 = seam.pipe2;
    auto delta_w = seam.deltaW;
    auto bw = seam.bw;
    auto Dn = pipe1.Dn;
    auto t1 = pipe1.t;
    auto t2 = pipe2.t;
    // FIXME v?
    auto v = seam.v;

    float alfa_m_t = 0;
    // есть смещение кромок
    if (!bw) {
        auto beta = (1.82*bw/sqrt(Dn*t1))*(1/(1+pow((t2/t1), 2.5)));
        auto alfa_m_t = 1 + (6*delta_w/t1)*(1/(1+(t2/t1)))*pow(M_E, -beta);
    }
    // сварка труб одинаковой толщины
    else if (t2 == t1) {
        auto t = t1;
        auto alfa_m_t = 1 + (3*delta_w)/t;
    }
    // сварка труб разной толщины
    else {
        if (t2 < t1) std::swap(t2,t1);
        auto n = 1.5;
        auto tn1 = pow(t1, n);
        auto tn2 = pow(t2, n);
        auto alfa_m_t = 1 + ((6*delta_w)/t1)*(tn1/(tn1 + tn2));
    }
    auto alfa_m_l = 1 + v*(alfa_m_t - 1);
    auto sigma_teta = 0;
    auto sigma_x = 0;
    return {sigma_x, sigma_teta};
}
/* Остаточные сварочные напряжения
 * sigma_R_x - продольная компонента
 * sigma_R_teta - кольцевая компонента */
sigma_x_t calcWeldingStress(double sigmaB02, double sigmaW02)
{
    /* sigma B0,2 – условный предел текучести основного металла трубы, МПа;
     * sigma W0,2 – условный предел текучести металла сварного шва, МПа;*/
    auto sigmaRx = std::min(sigmaB02, sigmaW02);
    auto sigma02 = std::max(sigmaB02, sigmaW02);
    auto sigmaRteta = sigma02;

    return {sigmaRx, sigmaRteta};
}
/* Расчетные напряжения 1 типа на пластическое разрушение
 * sigma_P_x - продольная компонента
 * sigma_P_teta - кольцевая компонента */
sigma_x_t calcStressDuctile(double sigmaNx, double sigmaNteta, double nL, double alfaMT)
{
    auto sigmaPx = sigmaNx*nL*(alfaMT-1);
    auto sigmaPteta = sigmaNteta*nL*(alfaMT-1);

    return {sigmaPx, sigmaPteta};
}
/* Расчетные напряжения 2 типа на трещиностойкость
 * sigma_F_x - продольная компонента
 * sigma_F_teta - кольцевая компонента */
sigma_x_t calcStressCrack(double sigmaPx, double sigmaPteta, double sigmaRx, double sigmaRteta)
{
    auto sigmaFx = sigmaPx + sigmaRx;
    auto sigmaFteta = sigmaPteta + sigmaRteta;

    return {sigmaFx, sigmaFteta};
}

}

/* Приложение Д
 * Критерии статической прочности кольцевого стыкового сварного соединения
 * трубопроводов */
namespace D {
// Д.3 Расчетный критерий второго типа F(Lr)
template<typename T>
T F(int calc_type, T Lr, T sigma_mat02, T sigma_matB, T E) {
    auto Lrmax = (sigma_mat02 + sigma_matB)/(2*sigma_mat02);

    if (calc_type == 1) {
        return pow(1+0.5*Lr*Lr, -0.5)*(0.3+0.7*pow(M_E,-0.6*pow(Lr,6)));
    }
    else if (calc_type == 2) {
        if (Lr < 1) {
            return pow(1+0.5*Lr*Lr, -0.5)*(0.3+0.7*pow(M_E,-0.6*pow(Lr,6)));
        }
        else if (1 <= Lr && Lr < Lrmax) {
            auto N = 0.3*(1-sigma_mat02)/sigma_matB;
            auto deltaE = 0.0375*(1 - sigma_mat02/1000); // протяженность площадки текучести
            auto lambda = 1 + E*deltaE/sigma_mat02;

            return pow(lambda + (1/2*lambda), -0.5)*pow(Lr, (N-1)/2*N);
        }
    }
    return 0;
}

template<typename T>
T Kr(T Lr, T Lrmax)
{
    if (Lr <= Lrmax)
        return F(Lr, 0, 0);
    else if (Lr > Lrmax)
        return 0;
    return 0;
}
}

/* Приложение Ж
 * Правила назначения коэффициентов запаса */
namespace ZH {
// граничного коэффициента интенсивности напряжений
double Kmat_(double Kmat, double nF) {
    return Kmat/nF;
}
// граничного предела текучести
double sigma02_(double sigma02mat, double nsigma) {
    return sigma02mat/nsigma;
}
// продольные и кольцевые напряжения
double sigmaNx_(double sigmaNx) {
    return 1.1*sigmaNx;
}
double sigmaNteta_(double sigmaNteta) {
    return 1.1*sigmaNteta;
}
}

/* Приложение И
 * Рекомендации по экспериментальному определению механических свойств
 * основного металла труб и металла сварных соединений, выполненных
 * дуговыми методами сварки */
namespace I {
// И.9 эквивалентный критический коэффициент интенсивности напряжений
double Kmat(double KCV, double t)
{
    return ((12*sqrt(KCV*0.8))-20)*pow(0.025/(t/1000),0.25)+20; // МПа·м^0,5
}
}

/* Приложение Л
 * Алгоритм расчета коэффициентов интенсивности напряжений */
namespace L {
// Л.1.2 Базовый расчет коэффициентов интенсивности напряжений для внутренних
// эллиптических трещин
double K(double sigmaFxm, double a, double c, double h, double t) {
    auto gamma = (0.5-(h+a)/t)*(0.5-(h+a)/t);
    auto Y = (1.79-0.66*(a/c))/pow((1-pow(a/(h+a), 1.8)*(1-0.4*a/c-gamma)), 0.54);
    auto S = sigmaFxm;
    return Y*S*sqrt(a/1000);
}
}

/* Приложение М
 * Алгоритм расчета номинальных напряжений в нетто-сечении сварного соединения */
namespace M {
/* М.1 Расчет номинальных напряжений в нетто-сечении
 * при наличии внутренних эллиптических трещин */
double sigmaN(const PipeSeam &pipeSeam, Defect defect, double sigmaPxm)
{
    auto pipe1 = pipeSeam.pipe1;
    auto Dn = pipe1.Dn;
    auto t = pipe1.t;
    auto r = (Dn-2*t)/2;
    auto S = sigmaPxm;
    auto a = defect.a;
    auto c = defect.c;
    double up = 0;
    double down = 0;
    // Расчет номинальных напряжений в нетто-сечении при наличии внутренних эллиптических трещин
    if (defect.type_place != TYPE::IN) {
        up = M_PI*(1-(2*a/t)) + (2*2*a/t)*sin(c/r);
        down = (1-(2*a/t))*(M_PI-(c/r)*(2*a/t));
    }
    // Расчет номинальных напряжений в нетто-сечении кольцевого стыкового сварного
    // соединения при наличии поверхностных полуэллиптических трещин
    else {
        up = M_PI*(1-(a/t)) + (2*a/t)*sin(c/r);
        down = (1-(a/t))*(M_PI-(c/r)*(a/t));
    }
    assert(down > 0);
    return (S*up)/down;
}
/* М.2 Расчет номинальных напряжений в нетто-сечении
 * кольцевого стыкового сварного соединения
 * при наличии поверхностных полуэллиптических трещин*/
}
} // namespace sto715
#endif // STO715

