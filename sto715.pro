QT       += core gui
CONFIG   += c++11
QMAKE_CXXFLAGS += -Wall -Wextra -pedantic -Wno-long-long

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sto715
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    defect.cpp \
    models/modeldefect.cpp \
    models/modelpipe.cpp \
    models/modelpipeline.cpp \
    models/modelseam.cpp

HEADERS  += mainwindow.h \
    sto715.h \
    pipe_seam.h \
    defect.h \
    include/toml/toml.h \
    parse_helper.h \
    models/modeldefect.h \
    models/modelpipe.h \
    models/modelpipeline.h \
    models/modelseam.h

FORMS    += mainwindow.ui
