#include "modelseam.h"

ModelSeam::ModelSeam(QObject *parent)
    : QAbstractTableModel(parent)
{
}

void ModelSeam::setSeam(const PipeSeam &seam)
{
    beginResetModel();
    _seam = seam;
    endResetModel();
}

QVariant ModelSeam::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal) {
            switch (section)
            {
            case 0:
                return "Значение";
            case 1:
                return "Размерность";
            case 2:
                return "Описание";
            }
        }
        if (orientation == Qt::Vertical) {
            switch (section)
            {
            case 0:
                return "p";
            case 1:
                return "v";
            case 2:
                return "alfa_t";
            case 3:
                return "deltaT";
            case 4:
                return "ro";
            case 5:
                return "deltaW";
            case 6:
                return "bw";
            case 7:
                return "sigmaB02";
            case 8:
                return "sigmaW02";
            case 9:
                return "sigma02";
            case 10:
                return "KCV";
            case 11:
                return "E";
            case 12:
                return "calc_type";
            }
        }
    }
    return QVariant();
}

int ModelSeam::rowCount(const QModelIndex & /*parent*/) const
{
   return 13;
}

int ModelSeam::columnCount(const QModelIndex & /*parent*/) const
{
    return 3;
}

QVariant ModelSeam::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        switch (index.column())
        {
        case 0:
            switch (index.row())
            {
            case 0:
                return _seam.p;
            case 1:
                return _seam.v;
            case 2:
                return _seam.alfa_t;
            case 3:
                return _seam.deltaT;
            case 4:
                return _seam.ro;
            case 5:
                return _seam.deltaW;
            case 6:
                return _seam.bw;
            case 7:
                return _seam.sigmaB02;
            case 8:
                return _seam.sigmaW02;
            case 9:
                return _seam.sigma02;
            case 10:
                return _seam.KCV;
            case 11:
                return _seam.E;
            case 12:
                return _seam.calc_type;
            }
        case 1:
            switch (index.row())
            {
            case 0:
                return "МПа";
            case 1:
                return "";
            case 2:
                return "1/град";
            case 3:
                return "мм";
            case 4:
                return "мм";
            case 5:
                return "мм";
            case 6:
                return "мм";
            case 7:
                return "МПа";
            case 8:
                return "МПа";
            case 9:
                return "МПа";
            case 10:
                return "Дж/см2";
            case 11:
                return "МПа";
            case 12:
                return "";
            }
        case 2:
            switch (index.row())
            {
            case 0:
                return "Давление";
            case 1:
                return "Коэффициент Пуассона";
            case 2:
                return "Коэффициент линеного температурного расширения";
            case 3:
                return "Разнотолщинность";
            case 4:
                return "Радиус кривизны упругого изгиба";
            case 5:
                return "Смещение срединной поверхности трубы";
            case 6:
                return "Ширина шва, измеренная по наружной поверхности трубы";
            case 7:
                return "Предел прочности основного металла трубы";
            case 8:
                return "Предел текучести металла сварного шва";
            case 9:
                return "Предел текучести основного металла трубы";
            case 10:
                return "Ударная вязкость на образцах Шарпи";
            case 11:
                return "Модуль упругости";
            case 12:
                return "Тип расчетного критерия";
            }
        }
    }
    return QVariant();
}

bool ModelSeam::setData(const QModelIndex & index, const QVariant & value, int role)
{
    if (role == Qt::EditRole && index.column() == 0)
    {
        switch (index.row())
        {
        case 0:
            _seam.p = value.toDouble();
            break;
        case 1:
            _seam.v = value.toDouble();
            break;
        case 2:
            _seam.alfa_t = value.toDouble();
            break;
        case 3:
            _seam.deltaT = value.toDouble();
            break;
        case 4:
            _seam.ro = value.toDouble();
            break;
        case 5:
            _seam.deltaW = value.toDouble();
            break;
        case 6:
            _seam.bw = value.toDouble();
            break;
        case 7:
            _seam.sigmaB02 = value.toDouble();
            break;
        case 8:
            _seam.sigmaW02 = value.toDouble();
            break;
        case 9:
            _seam.sigma02 = value.toDouble();
            break;
        case 10:
            _seam.KCV = value.toDouble();
            break;
        case 11:
            _seam.E = value.toDouble();
            break;
        case 12:
            _seam.calc_type = value.toInt();
            break;
        }
    }
    return true;
}

Qt::ItemFlags ModelSeam::flags(const QModelIndex &index) const
{
    return Qt::ItemIsEditable | QAbstractTableModel::flags(index);
}
