#include "modelpipe.h"

ModelPipe::ModelPipe(QObject *parent)
    : QAbstractTableModel(parent)
{
}

void ModelPipe::setPipe(const Pipe &pipe)
{
    beginResetModel();
    _pipe = pipe;
    endResetModel();
}

QVariant ModelPipe::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal) {
            switch (section)
            {
            case 0:
                return "Значение";
            case 1:
                return "Размерность";
            case 2:
                return "Описание";
            }
        }
        if (orientation == Qt::Vertical) {
            switch (section)
            {
            case 0:
                return "t";
            case 1:
                return "Dn";
            case 2:
                return "E";
            }
        }
    }
    return QVariant();
}

int ModelPipe::rowCount(const QModelIndex & /*parent*/) const
{
    return 3;
}

int ModelPipe::columnCount(const QModelIndex & /*parent*/) const
{
    return 3;
}

QVariant ModelPipe::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        switch (index.column())
        {
        case 0:
            switch (index.row())
            {
            case 0:
                return _pipe.t;
            case 1:
                return _pipe.Dn;
            case 2:
                return _pipe.E;
            }
        case 1:
            switch (index.row())
            {
            case 0:
                return "мм";
            case 1:
                return "мм";
            case 2:
                return "МПа";
            }
        case 2:
            switch (index.row())
            {
            case 0:
                return "Толщина стенки трубы";
            case 1:
                return "Наружний диаметр трубы";
            case 2:
                return "Модуль упругости";
            }
        }
    }
    return QVariant();
}

bool ModelPipe::setData(const QModelIndex & index, const QVariant & value, int role)
{
    if (role == Qt::EditRole && index.column() == 0)
    {
        switch (index.row())
        {
        case 0:
            _pipe.t = value.toDouble();
            break;
        case 1:
            _pipe.Dn = value.toDouble();
            break;
        case 2:
            _pipe.E = value.toDouble();
            break;
        }
    }
    return true;
}

Qt::ItemFlags ModelPipe::flags(const QModelIndex &index) const
{
    return Qt::ItemIsEditable | QAbstractTableModel::flags(index);
}
