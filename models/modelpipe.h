#ifndef MODELPIPE_H
#define MODELPIPE_H

#include <QAbstractTableModel>
#include "../pipe_seam.h"

class ModelPipe : public QAbstractTableModel
{
public:
    ModelPipe(QObject *parent);
    void setPipe(const Pipe &pipe);
    Pipe getPipe() const { return _pipe; }

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE ;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
private:
    Pipe _pipe;
};

#endif // MODELPIPE_H
