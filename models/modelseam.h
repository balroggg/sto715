#ifndef MODELSEAM_H
#define MODELSEAM_H

#include <QAbstractTableModel>
#include "../pipe_seam.h"

class ModelSeam : public QAbstractTableModel
{
public:
    ModelSeam(QObject *parent);
    void setSeam(const PipeSeam &seam);
    PipeSeam getSeam() const { return _seam; }

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE ;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
private:
    PipeSeam _seam;
};

#endif // MODELSEAM_H
