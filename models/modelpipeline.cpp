#include "modelpipeline.h"

ModelPipeline::ModelPipeline(QObject *parent)
    : QAbstractTableModel(parent)
{

}

void ModelPipeline::setPipeline(const Piping &pipeline)
{
    beginResetModel();
    _pipeline = pipeline;
    endResetModel();
}

QVariant ModelPipeline::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal) {
            switch (section)
            {
            case 0:
                return "Значение";
            case 1:
                return "Описание";
            }
        }
        if (orientation == Qt::Vertical) {
            switch (section)
            {
            case 0:
                return "Категория";
            }
        }
    }
    return QVariant();
}

int ModelPipeline::rowCount(const QModelIndex & /*parent*/) const
{
   return 1;
}

int ModelPipeline::columnCount(const QModelIndex & /*parent*/) const
{
    return 2;
}

QVariant ModelPipeline::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (index.column() == 0)
            return _pipeline.category;
        if (index.column() == 1)
            return "Значения: В, 1-4";
    }
    return QVariant();
}

bool ModelPipeline::setData(const QModelIndex & index, const QVariant & value, int role)
{
    if (role == Qt::EditRole && index.column() == 0)
    {
        switch (index.row())
        {
        case 0:
            _pipeline.category = value.toString();
            break;
        }
    }
    return true;
}

Qt::ItemFlags ModelPipeline::flags(const QModelIndex &index) const
{
    return Qt::ItemIsEditable | QAbstractTableModel::flags(index);
}

