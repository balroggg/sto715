#ifndef MODELPIPELINE_H
#define MODELPIPELINE_H

#include <QAbstractTableModel>
#include "../pipe_seam.h"

class ModelPipeline : public QAbstractTableModel
{
public:
    ModelPipeline(QObject *parent);
    void setPipeline(const Piping &pipeline);
    Piping getPipeline() const { return _pipeline; }

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE ;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
private:
    Piping _pipeline;
};

#endif // MODELPIPELINE_H
