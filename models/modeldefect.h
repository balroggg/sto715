#ifndef MODELDEFECT_H
#define MODELDEFECT_H

#include <QAbstractTableModel>
#include "../defect.h"

class ModelDefect : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit ModelDefect(QObject *parent = 0);
    void setDefect(const Defect &defect);
    Defect getDefect() const { return _defect; }

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

private:
    Defect _defect;
};

#endif // MODELDEFECT_H
