#include "modeldefect.h"

ModelDefect::ModelDefect(QObject *parent)
    : QAbstractTableModel(parent)
{

}

void ModelDefect::setDefect(const Defect &defect)
{
    beginResetModel();
    _defect = defect;
    endResetModel();
}

QVariant ModelDefect::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal) {
            switch (section)
            {
            case 0:
                return "Значение";
            case 1:
                return "Размерность";
            case 2:
                return "Описание";
            }
        }
        if (orientation == Qt::Vertical) {
            switch (section)
            {
            case 0:
                return "type";
            case 1:
                return "F";
            case 2:
                return "h1";
            case 3:
                return "h2";
            case 4:
                return "ll";
            case 5:
                return "lt";
            case 6:
                return "h";
            case 7:
                return "x0";
            case 8:
                return "y0";
            }
        }
    }
    return QVariant();
}

int ModelDefect::rowCount(const QModelIndex &) const
{
    return 9;
}

int ModelDefect::columnCount(const QModelIndex &) const
{
    return 3;
}

QVariant ModelDefect::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        switch (index.column())
        {
        case 0:
            switch (index.row())
            {
            case 0:
                return _defect.getType();
            case 1:
                return _defect.F;
            case 2:
                return _defect.h1;
            case 3:
                return _defect.h2;
            case 4:
                return _defect.ll;
            case 5:
                return _defect.lt;
            case 6:
                return _defect.h;
            case 7:
                return _defect.x0;
            case 8:
                return _defect.y0;
            }
        case 1:
            switch (index.row())
            {
            case 0:
                return "";
            case 1:
                return "мм2";
            case 2:
                return "мм";
            case 3:
                return "мм";
            case 4:
                return "мм";
            case 5:
                return "мм";
            case 6:
                return "мм";
            case 7:
                return "мм";
            case 8:
                return "мм";
            }
        case 2:
            switch (index.row())
            {
            case 0:
                return "Тип дефекта (Внутренний дефект - 1, Поверхностный дефект - 2, Сквозной дефект - 3)";
            case 1:
                return "Площадь проекции дефекта (площадь дефекта)";
            case 2:
                return "Максимальная длина дефекта";
            case 3:
                return "Максимальная ширина дефекта перпендикулярно максимальной длине дефекта";
            case 4:
                return "Длина дефекта";
            case 5:
                return "Ширина дефекта";
            case 6:
                return "Глубина дефекта";
            case 7:
                return "Начало дефекта";
            case 8:
                return "Начало дефекта";
            }
        }
    }
    return QVariant();
}

bool ModelDefect::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Qt::EditRole && index.column() == 0)
    {
        switch (index.row())
        {
        case 0:
            _defect.setType(value.toInt());
            break;
        case 1:
            _defect.F = value.toDouble();
            break;
        case 2:
            _defect.h1 = value.toDouble();
            break;
        case 3:
            _defect.h2 = value.toDouble();
            break;
        case 4:
            _defect.ll = value.toDouble();
            break;
        case 5:
            _defect.lt = value.toDouble();
            break;
        case 6:
            _defect.h = value.toDouble();
            break;
        case 7:
            _defect.x0 = value.toDouble();
            break;
        case 8:
            _defect.y0 = value.toDouble();
            break;
        }
    }
    return false;
}

Qt::ItemFlags ModelDefect::flags(const QModelIndex &index) const
{
    return Qt::ItemIsEditable | QAbstractTableModel::flags(index);
}
