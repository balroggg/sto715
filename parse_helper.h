#ifndef PARSE_HELPER_H
#define PARSE_HELPER_H

#include "pipe_seam.h"
#include "defect.h"

//#include <include/toml/toml.h>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>

#include <QMessageBox>

#include <fstream>

namespace parse_helper {
/*
namespace parse_toml {
// parse pipe toml
Pipe parsePipe(const toml::Value* value)
{
    if (!value->valid())
        return {};

    auto keys = {
        "t",
        "Dn",
        "E"
    };
    for (auto key : keys) {
        if (!value->find(key)) {
            QMessageBox msgBox;
            QString text = QString("Нет описания параметра трубы:\n%1\nЗагрузка прервана.").arg(key);
            msgBox.setText(text);
            msgBox.exec();
            return {};
        }
    }

    Pipe pipe;
    auto obj = value->find("t");
    if (obj && obj->as<double>()) {
        pipe.t = { obj->as<double>() };
    }
    obj = value->find("Dn");
    if (obj && obj->as<int>()) {
        pipe.Dn = { obj->as<int>() };
    }
    obj = value->find("E");
    if (obj && obj->as<int>()) {
        pipe.E = { obj->as<int>() };
    }

    bool ok = pipe.t > 1;
    if (!ok) {
        QMessageBox msgBox;
        QString text = "Параметр \"t\" не может быть меньше 1 мм.\nЗагрузка прервана.";
        msgBox.setText(text);
        msgBox.exec();
        return {};
    }
    return pipe;
}
bool parseDefects(const toml::Value* value, PipeSeam &seam)
{
    if (!value->valid())
        return false;
    return true;
}
bool parseSeam(const toml::Value* val, PipeSeam &seam)
{
    return false;
}
// parse toml file
PipeSeam parseFile(const std::string &fname)
{
    std::ifstream ifs(fname);
    toml::Parser parser(ifs);

    toml::Value v = parser.parse();
    if (!v.valid()) {
        qDebug() << QString::fromStdString(parser.errorReason());
        return {};
    }

    const toml::Value* vPipe1 = v.find("pipe1");
    const toml::Value* vPipe2 = v.find("pipe2");
    const toml::Value* vSeam = v.find("seam");
//    const toml::Value* vDefs = v.find("defects");
    PipeSeam seam;
    seam.pipe1 = parsePipe(vPipe1);
    seam.pipe2 = parsePipe(vPipe2);
//    seam.defects = parseDefects(defects);
//    seam = parseSeam(seam);
    return seam;
}
}*/
namespace parse_json {
QJsonDocument parseFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return QJsonDocument();
    return QJsonDocument::fromJson(file.readAll());
}
//
bool parsePipe(const QJsonObject &obj, Pipe &pipe)
{
    if (obj.isEmpty())
        return false;

    auto keys = {
        "t",
        "Dn",
        "E"
    };
    for (auto key : keys) {
        if (!obj.contains(key)) {
            QMessageBox msgBox;
            QString text = QString("Нет описания параметра трубы:\n%1\nЗагрузка прервана.").arg(key);
            msgBox.setText(text);
            msgBox.exec();
            return false;
        }
    }

    pipe.t = {obj["t"].toDouble()};
    pipe.Dn = {obj["Dn"].toDouble()};
    pipe.E = {obj["E"].toDouble()};

    bool ok = pipe.t > 1;
    if (!ok) {
        QMessageBox msgBox;
        QString text = "Параметр \"t\" не может быть меньше 1 мм.\nЗагрузка прервана.";
        msgBox.setText(text);
        msgBox.exec();
    }
    return ok;
}
//
bool parseDefects(const QJsonArray &obj, PipeSeam &seam)
{
    if (obj.isEmpty())
        return true;
    std::vector<Defect> defects;
    for (int i = 0; i < obj.size(); ++i) {
        if (!obj.at(i).isObject())
            return false;
        QJsonObject d = obj.at(i).toObject();
        if (d.isEmpty())
            return false;
        auto keys = {
            "number",
            "type_place",
            "F",
            "h1",
            "h2",
            "ll",
            "lt",
            "h",
            "x0",
            "y0"
        };
        for (auto key : keys) {
            if (!d.contains(key)) {
                QMessageBox msgBox;
                QString text = QString("Нет описания параметра дефекта:\n%1\nЗагрузка прервана.").arg(key);
                msgBox.setText(text);
                msgBox.exec();
                return false;
            }
        }
        TYPE type;
        QString typeString = d["type_place"].toString();
        if (typeString == "Внутренний дефект")
            type = TYPE::IN;
        else if (typeString == "Поверхностный дефект")
            type = TYPE::SURFACE;
        else if (typeString == "Сквозной дефект")
            type = TYPE::THROUGH;
        defects.push_back({
                              type,
                              d["F"].toDouble(),
                              d["h1"].toDouble(),
                              d["h2"].toDouble(),
                              d["ll"].toDouble(),
                              d["lt"].toDouble(),
                              d["h"].toDouble(),
                              d["x0"].toDouble(),
                              d["y0"].toDouble()
                          });
        defects.back().setNumber(d["number"].toInt());
    }
    seam.defects = defects;
    return true;
}
//
bool parseSeam(const QJsonObject &obj, PipeSeam &seam)
{
    if (obj.isEmpty())
        return false;

    auto keys = {"p",
                 "v",
                 "alfa_t",
                 "deltaT",
                 "ro",
                 "deltaW",
                 "bw",
                 "sigmaB02",
                 "sigmaW02",
                 "sigma02",
                 "KCV",
                 "E",
                 "calc_type"
                };
    for (auto key : keys) {
        if (!obj.contains(key)) {
            QMessageBox msgBox;
            QString text = QString("Нет описания параметра шва:\n%1\nЗагрузка прервана.").arg(key);
            msgBox.setText(text);
            msgBox.exec();
            return false;
        }
    }
    seam.p = {obj["p"].toDouble()};
    seam.v = {obj["v"].toDouble()};
    seam.alfa_t = {obj["alfa_t"].toDouble()};
    seam.deltaT = {obj["deltaT"].toDouble()};
    seam.ro = {obj["ro"].toDouble()};
    seam.deltaW = {obj["deltaW"].toDouble()};
    seam.bw = {obj["bw"].toDouble()};
    seam.sigmaB02 = {obj["sigmaB02"].toDouble()};
    seam.sigmaW02 = {obj["sigmaW02"].toDouble()};
    seam.sigma02 = {obj["sigma02"].toDouble()};
    seam.KCV = {obj["KCV"].toDouble()};
    seam.E = {obj["E"].toDouble()};
    seam.calc_type = obj["calc_type"].toInt();

    bool ok = seam.ro > 1;
    if (!ok) {
        QMessageBox msgBox;
        QString text = "Параметр \"ro\" не может быть меньше 1 м.\nЗагрузка прервана.";
        msgBox.setText(text);
        msgBox.exec();
    }

    return true;
}
//
bool parsePiping(const QJsonObject &obj, PipeSeam &seam)
{
    if (obj.isEmpty())
        return false;

    auto keys = {"category"
                };
    for (auto key : keys) {
        if (!obj.contains(key)) {
            QMessageBox msgBox;
            QString text = QString("Нет описания параметра шва:\n%1\nЗагрузка прервана.").arg(key);
            msgBox.setText(text);
            msgBox.exec();
            return false;
        }
    }
    seam.piping.category = {obj["category"].toString("1")};

    return true;
}
//
PipeSeam parseDoc(const QJsonDocument &doc)
{
    if (doc.isNull()) {
        QMessageBox box;
        box.setText("Неправильный формат файла описания шва, проверьте файл.");
        box.exec();
        return PipeSeam();
    }
    QJsonObject obj = doc.object();
    if (obj.isEmpty())
        return PipeSeam();

    QJsonObject pipe1 = obj["pipe1"].toObject();
    QJsonObject pipe2 = obj["pipe2"].toObject();
    QJsonArray defects = obj["defects"].toArray();
    QJsonObject seam = obj["seam"].toObject();
    QJsonObject piping = obj["piping"].toObject();

    PipeSeam pipeSeam;
    bool ok1 = parsePipe(pipe1, pipeSeam.pipe1);
    bool ok2 = parsePipe(pipe2, pipeSeam.pipe2);
    bool ok3 = parseDefects(defects, pipeSeam);
    bool ok4 = parseSeam(seam, pipeSeam);
    bool ok5 = parsePiping(piping, pipeSeam);
    auto ok = ok1 && ok2 && ok3 && ok4 && ok5;

    return ok?pipeSeam:PipeSeam();
}
}
//
// parse file
PipeSeam parseFile(const QString &fname) {
//    if (fname.contains("toml"))
//        return parse_toml::parseFile(fname.toStdString());
    if (fname.contains("json")) {
        auto doc = parse_json::parseFile(fname);
        return parse_json::parseDoc(doc);
    }
    return {};
}
// parse files
QVector<PipeSeam> parseFiles(const QStringList &files)
{
    QVector<PipeSeam> seams;
    for (auto file : files) {
        PipeSeam pipeSeam = parseFile(file);
        if (!pipeSeam.isEmpty()) {
            seams << pipeSeam;
        }
    }
    return seams;
}
}
#endif // PARSE_HELPER_H
