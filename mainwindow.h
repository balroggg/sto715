#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidget>
#include <QVector>
#include <QKeyEvent>

#include "pipe_seam.h"
#include "models/modelpipe.h"
#include "models/modelpipeline.h"
#include "models/modelseam.h"
#include "models/modeldefect.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void readFiles();
    void calc();
    void save();
    void showResult(const QVector<PipeSeam> &seams);
    void addDefectRow(const Defect &defect);
    void fillTables(const PipeSeam &seam);
    void fillPipeTable(QTableWidget *table, const Pipe &pipe);
    void addEmptyDefect();
    void addDefect(const Defect &defect);
    void createModelDefect(const Defect &defect);
    void removeDefect();
    void initModels();
    void initViews();

protected:
    void keyReleaseEvent(QKeyEvent *event);

private:
    Ui::MainWindow *ui;
    // models
    ModelPipe *modelPipe1, *modelPipe2;
    ModelPipeline *modelPipeline;
    ModelSeam *modelSeam;
    QMap<int, ModelDefect*> modelsDefects;

    void savePipes(QJsonObject &json);
    void savePipeline(QJsonObject &json);
    void saveSeam(QJsonObject &json);
    void saveDefects(QJsonObject &json);
    QVector<PipeSeam> uniteDefects(const QVector<PipeSeam> &seams);
    QVector<PipeSeam> getSeams();
};

#endif // MAINWINDOW_H
