# README #

### Инструкция

1. Открыть исходные данные в виде json файлов описания шва и дефектов (пока только 1 файл, можно несколько)
2. Сделать расчет
3. Просмотреть результаты

### Structure of core items

[pipe seam description](https://bitbucket.org/balroggg/sto715/src/40c7b6667172af46576d72f8a1a269d044862762/pipe_seam.h?at=master&fileviewer=file-view-default)

[defect description](https://bitbucket.org/balroggg/sto715/src/021682c1e78a041ca26e24414dd4fb06ece85417/defect.h?at=master&fileviewer=file-view-default)

### Example of json seam
~~~~
{
  "pipe1": {
    "t": 15.7,
    "Dn": 1420,
    "E": 210000
  },
  "pipe2": {
    "t": 15.7,
    "Dn": 1420,
    "E": 210000  },
  "defects": [
    {
       "type_place": "in",
	   "F": 0,
	   "h1": 6,
	   "h2": 1.8,
       "ll": 6,
       "lt": 1.8,
       "h": 3,
       "x0": 0,
       "y0": 0
    }
  ],
  "piping": {
    "category": "2" 
  },
  "seam": {
    "p": 7.4,
    "v": 0.3,
    "alfa_t": 0.000012,
    "deltaT": -30,
    "ro": 1420000,
    "deltaW": 2,
    "bw": 0,
    "sigmaB02": 588,
    "sigmaW02": 450,
    "sigma02": 460,
    "KCV": 29,
    "E": 210000,
    "calc_type": 1
  }
}
~~~~
### Example of toml seam, no build for windows(
~~~~

# Шов №1

title = "Шов №1"

[pipe1]
t = 0
Dn = 0
E = 0

[pipe2]
t = 0
Dn = 0
E = 0

~~~~