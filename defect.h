#ifndef DEFECT_H
#define DEFECT_H
#include <QString>

/// Дефект

enum class TYPE { NONE, IN, SURFACE, THROUGH };
enum class TYPE_UNITE { NONE, SINGLE, UNITE };
enum class TYPE_STO { NONE, TOLERANCE, DANGER };

class Defect {
public:
    explicit Defect(int i = 0);
    Defect(TYPE type, double F_, double h1_, double h2_, double ll_, double lt_, double h_, double x0_, double y0_);
    Defect(const Defect &d1, const Defect &d2, double H_, double L_);
    QString getType() const;
    void setType(int i);
    void setType(const QString &type);
    void setNumber(int i) { _number = i; }
    int number() const { return _number; }
    TYPE type_place = TYPE::NONE;
    TYPE_UNITE type_unite = TYPE_UNITE::NONE;
    TYPE_STO type_sto = TYPE_STO::NONE;
    bool final = false;
    double a = 0, c = 0;   // полуоси эллипса описания дефекта
    double F = 0;          // площадь дефекта
    double h1 = 0;         // макс длина дефекта
    double h2 = 0;         // макс ширина дефекта перпендикулярно макс длине дефекта
    double ll = 0;         // длина дефекта
    double lt = 0;         // ширина дефекта
    double h = 0;          // глубина дефекта
    double x0 = 0, y0 = 0; // начало дефекта
    double H = 0, L = 0;   // эффективные размеры
    // результаты
    double Kr_ = -1;
    double Lr_ = -1;
    double K_ = -1;

private:
    int _number = 0;
};

#endif // DEFECT_H
