#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "sto715.h"
#include "parse_helper.h"

#include <QMessageBox>
#include <QFileDialog>
#include <QSettings>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);
    ui->pushButtonHome->hide();
    ui->pushButtonBack->hide();

    initModels();
    initViews();

    // show input data
    connect(ui->pushButtonData, &QPushButton::clicked, [this](){
        ui->pushButtonHome->show();
        ui->stackedWidget->setCurrentWidget(ui->page_inputdata); });
    connect(ui->pushButtonAddDefect,    &QPushButton::clicked, this, &MainWindow::addEmptyDefect);
    connect(ui->pushButtonRemoveDefect, &QPushButton::clicked, this, &MainWindow::removeDefect);
    connect(ui->pushButtonReadFiles,    &QPushButton::clicked, this, &MainWindow::readFiles);
    connect(ui->pushButtonCalc,         &QPushButton::clicked, this, &MainWindow::calc);
    connect(ui->pushButtonSave,         &QPushButton::clicked, this, &MainWindow::save);
    connect(ui->actionAbout_sto715,     &QAction::triggered, [this](){
        QMessageBox::information(this, "Справка", "Приложение для расчета допустимости дефектов сварных швов по СТО 715"); });

    // show results
    connect(ui->pushButtonShowResult, &QPushButton::clicked, [this](){
        ui->pushButtonHome->show();
        ui->pushButtonBack->hide();
        ui->stackedWidget->setCurrentWidget(ui->page_results); });
    // back home
    connect(ui->pushButtonHome, &QPushButton::clicked, [this](){
        ui->pushButtonHome->hide();
        ui->pushButtonBack->hide();
        ui->stackedWidget->setCurrentWidget(ui->page_main);
    });
    connect(ui->pushButton_defects, &QPushButton::clicked, [this](){
        ui->pushButtonHome->show();
        ui->pushButtonBack->show();
        ui->stackedWidget->setCurrentWidget(ui->page_defects);
    });
    connect(ui->pushButtonBack, &QPushButton::clicked, [this](){
        ui->pushButtonHome->show();
        ui->pushButtonBack->hide();
        ui->stackedWidget->setCurrentWidget(ui->page_inputdata);
    });

    // choose defect
    connect(ui->comboBoxChooseDefect, SIGNAL(currentIndexChanged(int)), ui->stackedWidget_Defects, SLOT(setCurrentIndex(int)));
    ui->stackedWidget_Defects->addWidget(new QWidget);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::readFiles()
{
    QSettings settings;
    QString path = settings.value("dir", QDir::currentPath()).toString();
    QStringList files = QFileDialog::getOpenFileNames(
                this,
                "Select one or more files to open",
                path,
                "Json files (*.json);;Toml files (*.toml);;All files (*.*)");
    if (!files.isEmpty())
        settings.setValue("dir", QFileInfo(files.first()).dir().absolutePath());
    auto seams = parse_helper::parseFiles(files);
    auto uniteSeams = uniteDefects(seams);
    if (!uniteSeams.isEmpty())
        fillTables(uniteSeams.first());
    QString text = QString("Read %1 seam(s) from %2 file(s)").arg(seams.size()).arg(files.size());
    ui->statusBar->showMessage(text);
}

QVector<PipeSeam> MainWindow::uniteDefects(const QVector<PipeSeam> &seams)
{
    QVector<PipeSeam> uniteSeams;
    for (auto seam : seams) {
        std::vector<Defect> defects = seam.defects;
        std::vector<Defect> uniteDefects;
        uint i = 0;
        uint j = 1;
        while (!defects.empty()) {
            // сравнили i defect со всеми
            auto d1 = defects.at(i);
            if (j == defects.size()) {
                if (d1.type_unite != TYPE_UNITE::UNITE) {
                    d1.type_unite = TYPE_UNITE::SINGLE;
                }
                defects.erase(defects.begin()+i);
                sto715::B::checkDefect(d1);
                uniteDefects.push_back(d1);
                i = 0;
                j = 1;
                continue;
            }

            auto d2 = defects.at(j);
            auto defect = sto715::B::checkDefect(d1, d2);
            if (defect.type_unite == TYPE_UNITE::UNITE) {
                defects.erase(defects.begin()+i);
                defects.erase(defects.begin()+j);
                sto715::B::checkDefect(defect);
                defects.push_back(defect);
                i = 0;
                j = 1;
                continue;
            }
            else {
                j++;
            }
        }
        uniteSeams << seam;
        uniteSeams.last().defects = uniteDefects;
        qDebug() << Q_FUNC_INFO << "Unite " << uniteDefects.size() << "defects";
    }
    QString text = QString("Read %1 seam(s)").arg(uniteSeams.size());
    ui->statusBar->showMessage(text);
    return uniteSeams;
}

QVector<PipeSeam> MainWindow::getSeams()
{
    QVector<PipeSeam> seams;
    PipeSeam seam = modelSeam->getSeam();
    seam.pipe1 = modelPipe1->getPipe();
    seam.pipe2 = modelPipe2->getPipe();
    seam.piping = modelPipeline->getPipeline();
    seam.defects.resize(modelsDefects.size());
    int i = 0;
    for (auto model : modelsDefects)
        seam.defects[i++] = model->getDefect();
    seams << seam;
    return seams;
}

void MainWindow::calc()
{
    auto seams = getSeams();
    auto uniteSeams = uniteDefects(seams);
    if (uniteSeams.isEmpty()) {
        QMessageBox::information(this, "!!!", "Отсутствуют дефекты для расчета.");
        return;
    }
    for (PipeSeam &seam : uniteSeams) {
        auto sigmaN_x_t = sto715::E::calcNominalStress(seam);
        auto sigmaR_x_t = sto715::E::calcWeldingStress(seam.sigmaB02, seam.sigmaW02);

        auto sigmaNx = sigmaN_x_t.first;
        auto sigmaNteta = sigmaN_x_t.second;
        const double nL = 1.1;      // Коэффициент запаса по эксплуатационным напряжениям
        auto nF = 1.7;              // Коэффициент запаса по трещиностойкости
        auto ns = 1.2;
        if (seam.piping.category == "1" || seam.piping.category == "2") {
            nF = 1.6;
            ns = 1.1;
        }
        else if (seam.piping.category == "3" || seam.piping.category == "4") {
            nF = 1.5;
            ns = 1.05;
        }
        double alfaMT = 1+ seam.v*(seam.alfa_t - 1);
        auto sigmaP_x_t = sto715::E::calcStressDuctile(sigmaNx, sigmaNteta, nL, alfaMT);

        double sigmaPx = sigmaP_x_t.first;
        double sigmaPteta = sigmaP_x_t.second;
        double sigmaRx = sigmaR_x_t.first;
        double sigmaRteta = sigmaR_x_t.second;
        auto sigmaF_x_t = sto715::E::calcStressCrack(sigmaPx, sigmaPteta, sigmaRx, sigmaRteta);
        auto sigmaFxm = sigmaF_x_t.first;
        //
        auto KCV = seam.KCV;
        auto t = seam.pipe1.t;
        auto Kmat = sto715::I::Kmat(KCV, t);
        auto Kmat_ = sto715::ZH::Kmat_(Kmat, nF);
        auto sigmaPxm = sigmaNx*nL;
        for (Defect &defect : seam.defects) {
            auto sigman = sto715::M::sigmaN(seam, defect, sigmaPxm);
            auto a = defect.a;
            auto c = defect.c;
            Q_ASSERT_X(a > 0 && c > 0, "null", "null a or c of defect ellipse");
            auto h = defect.h;
            auto t = defect.lt;
            auto K = sto715::L::K(sigmaFxm, a, c, h, t);
            defect.Kr_ = K/Kmat_;
            defect.Lr_ = ns*sigman/seam.sigmaW02;
            auto E = seam.E;
            defect.K_ = sto715::D::F(seam.calc_type, defect.Lr_, seam.sigma02, seam.sigmaB02, E);

            if (defect.Kr_ < defect.K_)
                defect.type_sto = TYPE_STO::TOLERANCE;
            else
                defect.type_sto = TYPE_STO::DANGER;
        }
    }
    showResult(uniteSeams);
    QString text = QString("Calc %1 unite seam(s)").arg(uniteSeams.size());
    ui->statusBar->showMessage(text);
    // show result
    ui->pushButtonHome->show();
    ui->stackedWidget->setCurrentWidget(ui->page_results);
}

void MainWindow::save()
{
    auto fname = QFileDialog::getSaveFileName(this, "Сохранить файл",
                                              QDir::currentPath() + "/untitled.json",
                                              "Json files (*.json)");
    if (fname.isEmpty())
        return;
    QFile file(fname);
    if (!file.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }
    QJsonObject json;

    savePipes(json);
    savePipeline(json);
    saveSeam(json);
    saveDefects(json);

    QJsonDocument doc(json);
    file.write(doc.toJson());
    file.close();
}

void MainWindow::addDefectRow(const Defect &defect)
{
    QString defectName = "Дефект №" + QString::number(defect.number());
    int c = 0;
    int rowCount = ui->tableWidgetResults->rowCount() + 1;
    int r = rowCount - 1;
    ui->tableWidgetResults->setRowCount(rowCount);
    ui->tableWidgetResults->setItem(r, c++, new QTableWidgetItem(defectName));
    ui->tableWidgetResults->setItem(r, c++, new QTableWidgetItem(QString::number(defect.Kr_)));
    ui->tableWidgetResults->setItem(r, c++, new QTableWidgetItem(QString::number(defect.Lr_)));
    ui->tableWidgetResults->setItem(r, c++, new QTableWidgetItem(QString::number(defect.K_)));
    QString text = (defect.type_sto == TYPE_STO::TOLERANCE)?"Допустим":"Не допустим";
    ui->tableWidgetResults->setItem(r, c++, new QTableWidgetItem(text));
}

void MainWindow::fillTables(const PipeSeam &seam)
{
    modelPipe1->setPipe(seam.pipe1);
    modelPipe2->setPipe(seam.pipe2);
    modelPipeline->setPipeline(seam.piping);
    modelSeam->setSeam(seam);
    for (auto m : modelsDefects)
        delete m;
    modelsDefects.clear();
    ui->comboBoxChooseDefect->clear();
    QLayoutItem *child;
    while ((child = ui->stackedWidget_Defects->layout()->takeAt(0)) != 0) {
        delete child;
    }
    for (const auto d : seam.defects) {
        addDefect(d);
    }
    ui->tableView_pipe1->setModel(modelPipe1);
    ui->tableView_pipe2->setModel(modelPipe2);
    ui->tableView_pipeline->setModel(modelPipeline);
    ui->tableView_seam->setModel(modelSeam);
}

void MainWindow::fillPipeTable(QTableWidget *table, const Pipe &pipe)
{
    table->setItem(0,0, new QTableWidgetItem(QString::number(pipe.t)));
    table->setItem(1,0, new QTableWidgetItem(QString::number(pipe.Dn)));
    table->setItem(2,0, new QTableWidgetItem(QString::number(pipe.E)));
}

void MainWindow::addEmptyDefect()
{
    auto number = ui->comboBoxChooseDefect->count();
    if (number > 5) {
        QMessageBox::information(this, "Предупреждение", "Максимальное количество дефектов: 5");
        return;
    }
    Defect defect(number);
    createModelDefect(defect);
}

void MainWindow::addDefect(const Defect &defect)
{
    createModelDefect(defect);
}

void MainWindow::createModelDefect(const Defect &defect)
{
    auto defectName = "Дефект №" + QString::number(defect.number());
    for (auto i = ui->comboBoxChooseDefect->count();i--;) {
        if (ui->comboBoxChooseDefect->currentText() == defectName) {
            QMessageBox::warning(this, "Предупреждение", "Одинаковые имена дефектов");
            return;
        }
    }
    QTableView *view = new QTableView(this);
    auto i = defect.number();
    if (!modelsDefects.contains(i))
        modelsDefects[i] = new ModelDefect(this);
    modelsDefects[i]->setDefect(defect);
    view->setModel(modelsDefects.value(i));
    view->resizeColumnsToContents();

    auto page = new QWidget();
    auto verticalLayout_page = new QVBoxLayout(page);
    verticalLayout_page->addWidget(view);

    ui->stackedWidget_Defects->addWidget(page);
    ui->comboBoxChooseDefect->addItem(defectName);
    ui->comboBoxChooseDefect->setCurrentIndex(defect.number());
}

void MainWindow::removeDefect()
{
    int i = ui->comboBoxChooseDefect->currentIndex();
    if (i == 0)
        return;
    ui->comboBoxChooseDefect->removeItem(i);
    auto w = ui->stackedWidget_Defects->widget(i);
    ui->stackedWidget_Defects->removeWidget(w);
}

void MainWindow::initModels()
{
    modelPipe1 = new ModelPipe(this);
    modelPipe2 = new ModelPipe(this);
    modelPipeline = new ModelPipeline(this);
    modelSeam = new ModelSeam(this);
}

void MainWindow::initViews()
{
    ui->tableView_pipe1->setModel(modelPipe1);
    ui->tableView_pipe2->setModel(modelPipe2);
    ui->tableView_pipeline->setModel(modelPipeline);
    ui->tableView_seam->setModel(modelSeam);

    ui->tableView_pipe1->resizeColumnsToContents();
    ui->tableView_pipe2->resizeColumnsToContents();
    ui->tableView_pipeline->resizeColumnsToContents();
    ui->tableView_seam->resizeColumnsToContents();
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    if (event->matches(QKeySequence::Close))
        close();
    QMainWindow::keyReleaseEvent(event);
}

void MainWindow::savePipes(QJsonObject &json)
{
    QJsonObject pipe1, pipe2;
    pipe1["t"] = modelPipe1->index(0,0).data().toDouble();
    pipe1["Dn"] = modelPipe1->index(1,0).data().toDouble();
    pipe1["E"] = modelPipe1->index(2,0).data().toDouble();

    pipe2["t"] = modelPipe2->index(0,0).data().toDouble();
    pipe2["Dn"] = modelPipe2->index(1,0).data().toDouble();
    pipe2["E"] = modelPipe2->index(2,0).data().toDouble();
    json["pipe1"] = pipe1;
    json["pipe2"] = pipe2;
}

void MainWindow::savePipeline(QJsonObject &json)
{
    QJsonObject pipeline;
    pipeline["category"] = modelPipeline->index(0,0).data().toString();
    json["piping"] = pipeline;
}

void MainWindow::saveSeam(QJsonObject &json)
{
    QJsonObject seam;
    seam["p"] = modelSeam->index(0,0).data().toDouble();
    seam["v"] = modelSeam->index(1,0).data().toDouble();
    seam["alfa_t"] = modelSeam->index(2,0).data().toDouble();
    seam["deltaT"] = modelSeam->index(3,0).data().toDouble();
    seam["ro"] = modelSeam->index(4,0).data().toDouble();
    seam["deltaW"] = modelSeam->index(5,0).data().toDouble();
    seam["bw"] = modelSeam->index(6,0).data().toDouble();
    seam["sigmaB02"] = modelSeam->index(7,0).data().toDouble();
    seam["sigmaW02"] = modelSeam->index(8,0).data().toDouble();
    seam["sigma02"] = modelSeam->index(9,0).data().toDouble();
    seam["KCV"] = modelSeam->index(10,0).data().toDouble();
    seam["E"] = modelSeam->index(11,0).data().toDouble();
    seam["calc_type"] = modelSeam->index(12,0).data().toInt();
    json["seam"] = seam;
}

void MainWindow::saveDefects(QJsonObject &json)
{
    QJsonArray defects;
    for (auto m :modelsDefects) {
        QJsonObject defect;
        defect["number"] = modelsDefects.key(m);
        defect["type_place"] = m->index(0,0).data().toString();
        defect["F"] = m->index(1,0).data().toDouble();
        defect["h1"] = m->index(2,0).data().toDouble();
        defect["h2"] = m->index(3,0).data().toDouble();
        defect["ll"] = m->index(4,0).data().toDouble();
        defect["lt"] = m->index(5,0).data().toDouble();
        defect["h"] = m->index(6,0).data().toDouble();
        defect["x0"] = m->index(7,0).data().toDouble();
        defect["y0"] = m->index(8,0).data().toDouble();
        defects.append(defect);
    }
    json["defects"] = defects;
}

void MainWindow::showResult(const QVector<PipeSeam> &seams)
{
    ui->tableWidgetResults->setRowCount(0);
    for (auto seam : seams) {
        for (auto defect : seam.defects) {
            addDefectRow(defect);
        }
    }
}
