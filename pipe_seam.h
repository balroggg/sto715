#ifndef PIPE_SEAM
#define PIPE_SEAM

#include <vector>
#include "defect.h"

/// Труба
struct Pipe {
    double t = 0;  // толщина стенки стыкуемых сваркой труб, мм
    int Dn = 0;    // наружний диаметрб мм
    int E = 0;     // модуль Юнга, МПа

    double getDvn()   // внутренний диаметрб мм
    { return (Dn - 2*t); }
};
/// Трубопровод
struct Piping {
    QString category = "1";
    float nF = 1.7;
    float nSigma = 1.2;
    float nL = 1.1;
};

/// Сварочное соединение
struct PipeSeam {
    bool isEmpty() const { return defects.empty(); }
    Pipe pipe1;
    Pipe pipe2;
    Piping piping;
    std::vector<Defect> defects;
    double p;           // давление газа, МПа
    double v;           // коэффициент Пуассона
    double alfa_t{0};   // коэф лин темп расширения, 1/град
    double deltaT;      // температурный перепад
    double ro{0};       // радиус кривизны упругого изгиба на участке расположения кольцевого сварного соединения, м
    double deltaW{0};   // смещение срединной поверхности трубы
    double bw = 0;      // ширина усиления сварного шва
    double sigmaB02{0}; // условный предел текучести основного металла трубы, МПа
    double sigmaW02{0}; // условный предел текучести металла сварного шва, МПа
    double sigma02{0};  // мин предел текучести
    double KCV;         // ударная вязкость на образцах Шарпи, Дж/см^2
    double E;           // модуль упругости, МПа
    int calc_type = 1; // тип расчетного критерия
};

#endif // PIPE_SEAM

