#include "defect.h"
#include <algorithm>

Defect::Defect(int i)
    : _number(i)
{}

Defect::Defect(TYPE type, double F_, double h1_, double h2_, double ll_, double lt_, double h_, double x0_, double y0_)
    : type_place(type), F(F_), h1(h1_), h2(h2_), ll(ll_), lt(lt_), h(h_), x0(x0_), y0(y0_)
{
    if (h1 <= 0)
        h1 = ll;
    if (h2 <= 0)
        h2 = lt;
}

Defect::Defect(const Defect &d1, const Defect &d2, double H_, double L_)
    : H(H_), L(L_)
{
    type_unite = TYPE_UNITE::UNITE;
    x0 = std::min(d1.x0, d2.x0);
    y0 = std::min(d1.y0, d2.y0);
}

QString Defect::getType() const {
    switch (type_place) {
    case TYPE::IN:
        return "Внутренний дефект";
    case TYPE::SURFACE:
        return "Поверхностный дефект";
    case TYPE::THROUGH:
        return "Сквозной дефект";
    default:
        break;
    }
    return "ERROR";
}

void Defect::setType(int i)
{
    switch (i) {
    case 1:
        type_place = TYPE::IN;
        break;
    case 2:
        type_place = TYPE::SURFACE;
        break;
    case 3:
        type_place = TYPE::THROUGH;
        break;
    default:
        type_place = TYPE::NONE;
        break;
    }
}

void Defect::setType(const QString &type)
{
    if (type == "Внутренний дефект")
        type_place = TYPE::IN;
    else if (type == "Поверхностный дефект")
        type_place = TYPE::SURFACE;
    else if (type == "Сквозной дефект")
        type_place = TYPE::THROUGH;
}
